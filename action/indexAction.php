<?php
	require_once("action/commonAction.php");
	require_once("action/dao/UserDAO.php");

	class LoginAction extends CommonAction {
		public $wrongLogin = false;
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_PUBLIC, "login");
		}

		protected function executeAction() {

			if (isset($_POST["username"])) {
				$data = [];
				$data["username"] = $_POST["username"];
				$data["pwd"] = $_POST["pwd"];
				
				$resultat = parent::callAPI("signin",$data);

				if (strlen($resultat) === 40) {
					$_SESSION["visibility"] = 1;
					$_SESSION["cleSession"] = $resultat;
						
					header("location:home.php");
					exit;
				}
				else {
					$this->wrongLogin = $resultat;
				}
			}
		}
	}
