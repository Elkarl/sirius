<?php
	require_once("action/CommonAction.php");

	class AjaxGameEnterAction extends CommonAction{
		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {
			if(isset($_POST['id']))
			{	
				$data = [];
				$data["key"] = $_SESSION["cleSession"];
				$data["id"] = $_POST["id"];

				$this->result = CommonAction::callAPI("enter", $data);
			}
			
		}
	}