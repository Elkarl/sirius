<?php

	require_once("action/CommonAction.php");
	// Démarrer une nouvelle session, ou recharger celle existante


class LogoutAction extends CommonAction{

	public function __construct(){
		parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
	}

	function executeAction() {
		
		$data = array("key" => $_SESSION["cleSession"]);
		CommonAction::callAPI("signout",$data);
		session_unset();
		session_destroy();

		header("location:index.php");
		exit;
	}
}