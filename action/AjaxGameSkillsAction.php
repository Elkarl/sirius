<?php
	require_once("action/CommonAction.php");

	class AjaxGameSkillsAction extends CommonAction{
		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {
			if(isset($_POST['skill-name']))
			{	
				$data = [];
				$data["key"] = $_SESSION["cleSession"];
				$data["skill-name"] = $_POST["skill-name"];

				$this->result = CommonAction::callAPI("action", $data);
			}
		}
	}