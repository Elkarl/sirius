<?php
	require_once("action/CommonAction.php");

	class AjaxUserAction extends CommonAction{
		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {
			$data = array("key" => $_SESSION["cleSession"]);
			$this->result = CommonAction::callAPI("user-info", $data);
		}
	}