<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Game's on</title>
	<link rel="stylesheet" href="css/jquery-ui.css">
	<link rel="stylesheet" href="css/jquery-ui.structure.css">
	<link rel="stylesheet" href="css/jquery-ui.theme.css">
	<link rel="stylesheet" href="css/global.css">
	<link rel="stylesheet" href="css/theme-ninja.css">
	<script src="js/jquery.js" charset="utf-8"></script>
	<script src="js/jquery-ui-1.12.1.custom/jquery-ui.js" charset="utf-8"></script>
	<script src="js/jsGame.js" charset="utf-8"></script>
	<script src="js/ninja/sprite/kyubi.js" charset="utf-8"></script>
	
	<script src="js/ninja/sprite/ally2.js" charset="utf-8"></script>
	<script src="js/ninja/sprite/ally3.js" charset="utf-8"></script>
	<script src="js/ninja/sprite/naruto.js" charset="utf-8"></script>
	<script src="js/ninja/sprite/narutoAttack.js" charset="utf-8"></script>
	
	<script src="js/ninja/sprite/ally1.js" charset="utf-8"></script>
	
	

	<script src="js/ninja/sprite/kyubiAttack.js" charset="utf-8"></script>
	<script src="js/TiledImage.js"></script>

	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
</head>
<body background="images/Finalvalley.png">

<div id="divGameContenu" style="display:none;">
	<div id="infoPartie" class="game-box-title" >BANNED :(</div>

	<div id="infoJoueurs" class="game-box-player" style="background-color:rgba(255,255,255,1);">
		<div id="playerName">BANNED :(</div>
		<div id="playerLevel">BANNED :(</div>
		<div id="playerType">BANNED :(</div>
		<div id="playerHp">BANNED :(</div>
		<div id="playerMp">BANNED :(</div>
	</div>
	<div id="infoOtherPlayer" style="display:none;">
			<div id="otherPlayerName"></div>
			<div id="otherPlayerLevel"></div>
			<div id="otherPlayerType"></div>
			<div id="otherPlayerHp"></div>
			<div id="otherPlayerMp"></div>
	</div>
	<div id="playerSkills" class="game-box-skills">
		<div id="skill0" class="skills skill-1"  style="display:none;">
			<div id="skill0Name">BANNED :(</div>	
			<div id="skill0Cost">BANNED :(</div>	
			<div id="skill0Dmg">BANNED :(</div>	
			<div id="skill0Heal">BANNED :(</div>	
		</div>		
		<div id="skill1" class="skills skill-2" style="display:none;">
			<div id="skill1Name">BANNED :(</div>	
			<div id="skill1Cost">BANNED :(</div>	
			<div id="skill1Dmg">BANNED :(</div>	
			<div id="skill1Heal">BANNED :(</div>	
		</div>		
		<div id="skill2" class="skills skill-3" style="display:none;">
			<div id="skill2Name">BANNED :(</div>	
			<div id="skill2Cost">BANNED :(</div>	
			<div id="skill2Dmg">BANNED :(</div>	
			<div id="skill2Heal">BANNED :(</div>	
		</div>		
	</div>
	<div id="bossInfo" class="game-box-boss">
		<div id="gameType">BANNED :(</div>
		<div id="gameLevel">BANNED :(</div>
		<div id="gameHp">BANNED :(</div>
	</div>
</div>
<div id="noMp" class="inGameMessage" style="display:none;">BANNED :(</div>

<div id="boxFinPartie" class="game-box-end" style="display:none;">

	BANNED v_v
	<ul>
		<li id="FenetreGame"><a href="home.php">Return home</a></li>
		<li id="quitterJeu"><a href="logout.php">Quit</a></li>
	</ul>
</div>

<div id="progressbar"><div class="progress-label">SKILL ON COOLDOWN</div></div>

<div id="containerCanevas">
	<canvas id="canvasGame" width="900" height="500"></canvas>
</div>


</body>
</html>
