<?php
	require_once("action/indexAction.php");

	$action = new LoginAction();
	$action->execute();

	$wrongLogin = $action->wrongLogin;

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Connexion |  Sirius</title>
        <script src="js/jquery.js" charset="utf-8"></script>
		<script src="js/javascript.js" charset="utf-8"></script>
		<script src="js/TiledImage.js"></script>
        <link rel="stylesheet" href="css/global.css">
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <!-- <link rel="icon" href="images/favicon.ico" type="image/x-icon"> -->

        <link rel="stylesheet" href="css/theme-ninja.css">

		<!-- A mettre dans un autre fichier -->
		<script>
			function saveUsername() {
				localStorage["username"] = $("#textAreaUsername").val();
			}

			window.onload = function () {
				if (localStorage["username"] != null) {
					$("#textAreaUsername").val(localStorage["username"]);
				}
			}
		</script>

 
    </head>
    <body>
        <div class="main">
                        
            <div class="splash">
    <div class="content-box">
        <h1>Connexion à Sirius</h1>
        <form action="index.php" method="post">
			<div class="response-error"><?=$wrongLogin ?></div>
            <div>
                <label>Nom d'usager</label>
                <input id="textAreaUsername" type="text" name="username" value="">
            </div>
            <div>
                <label>Mot de passe</label>
                <input type="password" name="pwd" value="">
            </div>
            <div>
                <button id="bouttonSubmit" type="submit" onclick="saveUsername()">Entrer</button>
            </div>
        </form>
    </div>
</div>
            
        </div>
        <canvas id="canvas"></canvas>
    </body>
</html>
