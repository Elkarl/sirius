<?php
	session_start();

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Accueil |  Sirius</title>
		<script src="js/jquery.js" charset="utf-8"></script>
		<script src="js/jsHome.js" charset="utf-8"></script>
		<script src="js/TiledImage.js"></script>
		<link rel="stylesheet" href="css/global.css">
		<link rel="stylesheet" href="css/theme-ninja.css">
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    </head>
    <body>
        <div class = "">
            <header>
			<!-- <div><?=$_SESSION["cleSession"] ?></div> -->
                <ul>
					<li id="char-button">Personnages</li>
					<li id="games-button">Parties</li>
					<li id="logout-button"><a href="logout.php">Déconnexion</a></li>
				</ul>
            </header>
            

	<div id="divContenu" class ="content-box" style="display:none">
	<div id="reponseErreur" class="response-error" style="display:none">Game_status</div>
		<!-- SECTION INFORMATION PERSONNAGE -->
		<div id="informationPerso">	
			<div id="nomSection"></div>
			<div id="divUsername"></div>
			<div id="divHP"></div>
			<div id="divMP"></div>
			<div id="divLevel"></div>

			<div id="divXp"></div>
			<div id="divVictories"></div>
			<div id="divLoss"></div>

			<!-- <div id="divUnspentPoint"></div>
			<div id="divUnspentSkills"></div> -->
			<div id="divdDodgeChance"></div>
			<div id="divDmgRed"></div>
		</div>
		<div id="informationPartie">

		</div>
	</div>
			
	<canvas id="canvasHome"></canvas>

    </body>
</html>
