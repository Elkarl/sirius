<?php 
	require_once("action/AjaxGameSkillsAction.php");

	$action = new AjaxGameSkillsAction();
	$action->execute();

	echo json_encode($action->result);