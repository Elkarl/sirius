<?php 
	require_once("action/AjaxGameStatusAction.php");

	$action = new AjaxGameStatusAction();
	$action->execute();

	echo json_encode($action->result);