var ctx = null;
var canvasWidth;
var canvasHeight;
let sprite;
let positionShurikenY = -50;
let positionShurikenX = (Math.random()* 1500) +120 ;
let cooldownRefresh = Date.now();
let musicHome = new Audio("mp3/Pain.mp3");



$(function() {
	
	// if (localStorage["username"] != null) {
	// 	$("#textAreaUsername").val(localStorage["username"]);
	// }

	document.getElementById("char-button").onclick = function(){
		
		document.getElementById("divContenu").style.display = "block";
		document.getElementById("informationPerso").style.display = "block";
		document.getElementById("informationPartie").style.display = "none";
	}
	
	document.getElementById("games-button").onclick = function(){
		document.getElementById("divContenu").style.display = "block";
		document.getElementById("informationPerso").style.display = "none";
		document.getElementById("informationPartie").style.display = "block";
		document.getElementById("reponseErreur").style.display = "none";
	}

	setTimeout(function refreshChar(){
		let username;
		let type;
		let level;
		let base_level_exp;
		let next_level_exp;
		let exp;
		let victories;
		let loss;
		let last_game_state;
		let unspent_points;
		let unspent_skills;
		let welcome_text;
		let hp;
		let mp;
		let dodge_chance;
		let dmg_red;
		document.getElementById("reponseErreur").style.display = "none";
		if(Date.now() > cooldownRefresh + 2000 )
		{	
			$.ajax({
				url:"ajax-user-info.php",
				type: "POST",
				data:{
					"username" : username,
					"type" : type,
					"level" : level,
					"base_level_exp" : base_level_exp,
					"next_level_exp" : next_level_exp,
					"exp" : exp,
					"victories" : victories,
					"loss" : loss,
					"last_game_state" : last_game_state,
					"unspent_points" : unspent_points,
					"unspent_skills" : unspent_skills,
					"welcome_text" : welcome_text,
					"hp" : hp,
					"mp" : mp,
					"dodge_chance" : dodge_chance,
					"dmg_red" : dmg_red			
				}

			})
			.done(function (data){

				cooldownRefresh = Date.now();
				let information = JSON.parse(data);

					document.getElementById("nomSection").innerHTML = "information du joueur :";
					document.getElementById("divUsername").innerHTML = "Nom : " + information.username;
					document.getElementById("divHP").innerHTML = "Vie : " + information.hp;
					document.getElementById("divMP").innerHTML = "Mana : " + information.mp;
					//document.getElementById("divType").innerHTML = "Type : " + information.type;
					document.getElementById("divLevel").innerHTML = "Niveau : " + information.level;
					// document.getElementById("divXpBase").innerHTML = "Experience de base du niveau : " + information.base_level_exp;
					// document.getElementById("divXpNextLevel").innerHTML = "Experience jusqu'au prochain niveau : " + information.next_level_exp;
					document.getElementById("divXp").innerHTML = "Experience : " + information.exp + "/"+ information.next_level_exp;
					document.getElementById("divVictories").innerHTML = "Victoires : " + information.victories;
					document.getElementById("divLoss").innerHTML = "Défaites : " + information.loss;
					// document.getElementById("divLastGameStat").innerHTML = "nom : " + information.last_game_state;
					// document.getElementById("divUnspentPoint").innerHTML = "nom : " + information.unspent_points;
					// document.getElementById("divUnspentSkills").innerHTML = "nom : " + information.unspent_skills;
					document.getElementById("divdDodgeChance").innerHTML = "Chance de feintes : " + information.dodge_chance +"%";
					document.getElementById("divDmgRed").innerHTML = "Armure : " + information.dmg_red +"%";

					setTimeout(refreshChar,2000);
			})
		}
	},2000)
	
		
	setTimeout(function refreshGames(){
		let name;
		let level;
		let current_hp;
		let max_users;
		let type;
		let users;
		let id;
		if(Date.now() > cooldownRefresh + 2000 )
		{	
			$.ajax({
				url:"ajax-game-info.php",
				type: "POST",
				data:{
					"name" : name,
					"level" : level,
					"current_hp" : current_hp,
					"max_users" : max_users,
					"type" : type,
					"users" : users,
					"id" : id
				}
	
			})
			.done(function (data){

				cooldownRefresh = Date.now();
				let information = JSON.parse(data);
				let text;
				let newNode;
				let newTextNode;
				let parentNode;
				let usersLength;
	


	
				parentNode = document.getElementById("informationPartie");
				parentNode.innerHTML = "<div id='informationPartie'></div>"; 
				for(let i =0; i < information.length ; i++)
				{
					if(information[i].users != undefined){
						usersLength = information[i].users.length;
					}
					else{
						usersLength = 0;
					}
					// VERSION DOM
					// text = 	information[i].name + "<br>" 
					// 		+ information[i].level + "<br>" 
					// 		+ "0/" + information[i].max_users +"<br>"
					// 		+ information[i].current_hp +"<br>" 
					// 		+ information[i].type ;
					// newNode = document.createElement("div");
					// newTextNode = document.createTextNode(text);
					// newNode.appendChild(newTextNode);
					// parentNode = document.getElementById("informationPartie");
					// parentNode.appendChild(newNode);
	
					//VERSION INNERHTML
					text = 	"<p>" + information[i].name + "<br>" 
							+ "<span>" + information[i].level + "</span>" + "<br>" 
							+ usersLength
							+ "/" + information[i].max_users +"<br>"
							+ information[i].current_hp +"<br>" 
							+ information[i].type +"</p>";
					
					
					parentNode.innerHTML = parentNode.innerHTML 
							+ "<div id='partie_" + information[i].id + "' class='game-box' onclick='OpenNewTab("+information[i].id+")'>" 
							+ text 
							+ "</div>";		
	
				}
				parentNode.innerHTML = parentNode.innerHTML + "<div class='clear'></div>";

								setTimeout(refreshGames,2000);
	
			})
		}
	},2000)
		


	

	

	//Exemple des informations dans un tableau



	// =======================================================
	// Animation in a canvas
	// -------------------------------------------------------
	let columnCount = 6;
	let rowCount = 1;
	let refreshDelay = 1; 			// msec
	let loopColumns = true; 			// or by row?
	let scale = 2.1;
	sprite = new TiledImage("images/shuriken.png", columnCount, rowCount, refreshDelay, loopColumns, scale, null);
	sprite.changeRow(0);				// One row per animation
	sprite.changeMinMaxInterval(1, 6); 	// Loop from which column to which column?

	// Other functions : 
	//   - sprite.setFlipped(true);		// Horizontally
	//   - sprite.setLooped(false);

	ctx = document.getElementById("canvasHome").getContext("2d");
	
	//Faire une classe player et codé lui assigné la tiledImage à cet endroit. 
	//Ensuite faire runner le tick a partir de la classe player (comme pour les ball etc)

    canvasHome.width = window.innerWidth;
    canvasHome.height = window.innerHeight;
    canvasHomeWidth = canvasHome.width;
	canvasHomeHeight = canvasHome.height;
	
	background = new Image();
	background.src = "images/Itachi.jpg";

	musicHome.play();
	musicHome.volume = 0.05;
	
	generalTick();
	
});	

// document.getElementById("bouttonSubmit").onclick = function(){
// 	console.log("1");
// 	localStorage["username"] = $("#textAreaUsername").val();
// }


function generalTick() {
	if (background.complete) {
		ctx.drawImage(background, 0, 0);
	}
	positionShurikenY += 10;
	if(positionShurikenY > 1500)
		{
			positionShurikenX = (Math.random()* 1500) +120 ;
			positionShurikenY = -50;
		}

	if(musicHome.ended)
		{
			musicHome.play();
		}
	sprite.tick(positionShurikenX, positionShurikenY, ctx);

	window.requestAnimationFrame(generalTick);
}

function OpenNewTab(id){
	setTimeout(function(){ 
		$.ajax({
			url:"ajax-game-enter.php",
			type: "POST",
			//dataType:'json', // add json datatype to get json
			data: {
				id : id
			}
		})
		.done(function (data){
			let information = JSON.parse(data);
			if(information != "GAME_ENTERED")
			{
				document.getElementById("reponseErreur").style.display = "block";
				document.getElementById("reponseErreur").innerHTML = information;	
			}
			else{
				window.location.replace("game.php");
			}
			

		})
		//window.location.href = "game.php"; 
	}, 500);

}


window.onresize = function(event) {
    canvasHome.width = window.innerWidth;
    canvasHome.height = window.innerHeight;
    canvasHomeWidth = canvasHome.width;
    canvasHomeHeight = canvasHome.height;
};


