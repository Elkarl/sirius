var ctx = null;
var canvasWidth;
var canvasHeight;
let sprite;
let musicIntro = new Audio("mp3/Yamagsumi.mp3");

$(function() {
	
	// if (localStorage["username"] != null) {
	// 	$("#textAreaUsername").val(localStorage["username"]);
	// }
	


	// =======================================================
	// Animation in a canvas
	// -------------------------------------------------------
	let columnCount = 6;
	let rowCount = 1;
	let refreshDelay = 100; 			// msec
	let loopColumns = true; 			// or by row?
	let scale = 2.1;
	sprite = new TiledImage("images/bandana.png", columnCount, rowCount, refreshDelay, loopColumns, scale, null);
	sprite.changeRow(0);				// One row per animation
	sprite.changeMinMaxInterval(1, 6); 	// Loop from which column to which column?

	// Other functions : 
	//   - sprite.setFlipped(true);		// Horizontally
	//   - sprite.setLooped(false);

	ctx = document.getElementById("canvas").getContext("2d");
	
	//Faire une classe player et codé lui assigné la tiledImage à cet endroit. 
	//Ensuite faire runner le tick a partir de la classe player (comme pour les ball etc)

    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    canvasWidth = canvas.width;
	canvasHeight = canvas.height;
	
	background = new Image();
	background.src = "images/background.jpg";

	ninja = new Image();
	ninja.src = "images/ninja.png";



	musicIntro.play();
	musicIntro.volume = 0.05;
	
	generalTick();
	
});	

// document.getElementById("bouttonSubmit").onclick = function(){
// 	console.log("1");
// 	localStorage["username"] = $("#textAreaUsername").val();
// }


function generalTick() {
	if (background.complete) {
		ctx.drawImage(background, 0, 0);

	}
	if(ninja.complete){
		ctx.drawImage(ninja,1000,350,450,450 *ninja.height / ninja.width);
	}
	if(musicIntro.ended)
	{
		musicIntro.play();
	}

	sprite.tick(1265, 585, ctx);

	window.requestAnimationFrame(generalTick);
}




window.onresize = function(event) {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    canvasWidth = canvas.width;
    canvasHeight = canvas.height;
};
