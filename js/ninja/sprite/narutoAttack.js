let positionXAttackNaruto = 255;

$(function() {
	
		// =======================================================
				// Animation in a canvas
				// -------------------------------------------------------
				let columnCount = 10;
				let rowCount = 2;
				let refreshDelay = 200; 			// msec
				let loopColumns = true; 			// or by row?
				let scale = 2.0;
				let sprite = new TiledImage("images/rasenShuriken.png", columnCount, rowCount, refreshDelay, loopColumns, scale, null);
				sprite.changeRow(1);				// One row per animation
				sprite.changeMinMaxInterval(1, 4); 	// Loop from which column to which column?
				let ctx = document.getElementById("canvasGame").getContext("2d");
	
				tick();
	
				function tick() {
				
					//animation d'attaque du boss
				if(rasenShurikenLance==true){
					if(positionXAttackNaruto < 750)
					{
						sprite.changeRow(1);				
						sprite.changeMinMaxInterval(1, 4);
					}
					if(positionXAttackNaruto > 750)
						{
							sprite.changeRow(0);				
							sprite.changeMinMaxInterval(1, 10);
							if(animationAttack > 125){
								rasenShurikenLance=false;
								animationAttack =0;
							}
						}
						else{
							positionXAttackNaruto += 15;
						}
					sprite.tick(positionXAttackNaruto, 400, ctx);
					animationAttack++;
				}
				else
				{
					positionXAttackNaruto = 255;
				}
	
	
					window.requestAnimationFrame(tick);
				}
	
	});	