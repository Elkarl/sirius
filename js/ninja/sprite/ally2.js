let positionXKizame = -200;
let enterGameKizame = true;
let positionPassifKizame = false;
let inGameKizame =false;
let leaveGameKizame = false;



$(function() {

	// =======================================================
			// Animation in a canvas
			// -------------------------------------------------------
			let columnCount = 13;
			let rowCount = 5;
			let refreshDelay = 100; 			// msec
			let loopColumns = true; 			// or by row?
			let scale = 1.75;
			let sprite = new TiledImage("images/kizameSprite.png", columnCount, rowCount, refreshDelay, loopColumns, scale, null);
			sprite.changeRow(0);				// One row per animation
			sprite.changeMinMaxInterval(0, 4); 	// Loop from which column to which column?
			let ctx = document.getElementById("canvasGame").getContext("2d");

			tick();

			function tick() {
				
			if(nbOtherPlayer > 1)
			{
				enterGameKizame = true;
				leaveGameKizame = false;
			}
			else{
				enterGameKizame = false;
				leaveGameKizame = true;
			}
			//Animation entré jeu
			if(positionXKizame < 140 && enterGameKizame == true){
				positionXKizame+=10;
				inGameKizame = true;
				sprite.setFlipped(false);
				sprite.tick(positionXKizame, 375, ctx);
			}

				//Variable passive a true
			if(positionXKizame >130 && positionXKizame < 150  && inGameKizame == true)
			{
				enterGameKizame = false;
			}

				//position passive (STANCE)
			if(positionXKizame >= 140 && animationMort != "GAME_NOT_FOUND_LOST" && leaveGameKizame == false && animationKizame==0){
				sprite.changeRow(1);
				sprite.changeMinMaxInterval(0,3);
				sprite.tick(positionXKizame, 375, ctx);			
			}	

			//animation d'attaque de Kizame
			if(animationKizame > 0 && enterGameKizame == false){
				sprite.changeRow(2);
				sprite.changeMinMaxInterval(0,12);
				sprite.tick(positionXKizame, 375, ctx);
				animationKizame++;
				if(animationKizame > 85)
				{
					animationKizame =0;
					
				}
			}

			//Player leave la game
			if(leaveGameKizame == true && positionXKizame > -200)
			{
				positionXKizame-=10;
				inGameKizame = false;
				sprite.changeRow(0);
				sprite.changeMinMaxInterval(0, 5);
				sprite.setFlipped(true);
				sprite.tick(positionXKizame, 375, ctx);
				
			}

			if(animationMort == "GAME_NOT_FOUND_LOST"){
				if(animationKizame < 15){
					animationKizame++
					sprite.changeRow(4);
					sprite.changeMinMaxInterval(0,4);
					sprite.tick(positionXKizame, 425, ctx);
				}
				else{
					sprite.changeRow(4);
					sprite.changeMinMaxInterval(4,4);
					sprite.tick(positionXKizame, 425, ctx);
				}
			}



				window.requestAnimationFrame(tick);
			}

});	


