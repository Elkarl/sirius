let positionXAttack = 750;

$(function() {
	
		// =======================================================
				// Animation in a canvas
				// -------------------------------------------------------
				let columnCount = 10;
				let rowCount = 6;
				let refreshDelay = 200; 			// msec
				let loopColumns = true; 			// or by row?
				let scale = 2.0;
				let sprite = new TiledImage("images/kyubiRunningFixed.png", columnCount, rowCount, refreshDelay, loopColumns, scale, null);
				sprite.changeRow(4);				// One row per animation
				sprite.changeMinMaxInterval(6, 9); 	// Loop from which column to which column?
				sprite.setFlipped(true);
				let ctx = document.getElementById("canvasGame").getContext("2d");
	
				tick();
	
				function tick() {
				
					//animation d'attaque du boss
				if(attackedBoss==true){
					if(positionXAttack == 0)
						{
							positionXAttack = 750;
						}
					positionXAttack -= 30;
					if(endGame == false){
						sprite.tick(positionXAttack, 400, ctx);
					}

				}
				else
					{
						positionXAttack = 750;
					}
	
	
					window.requestAnimationFrame(tick);
				}
	
	});	