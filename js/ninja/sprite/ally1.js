let positionXSasuke = -200;
let enterGameSasuke = false;
let positionPassifSasuke = false;
let inGameSasuke =false;
let leaveGameSasuke = false;



$(function() {

	// =======================================================
			// Animation in a canvas
			// -------------------------------------------------------
			let columnCount = 11;
			let rowCount = 4;
			let refreshDelay = 100; 			// msec
			let loopColumns = true; 			// or by row?
			let scale = 2.5;
			let sprite = new TiledImage("images/sasukeSprite.png", columnCount, rowCount, refreshDelay, loopColumns, scale, null);
			sprite.changeRow(0);				// One row per animation
			sprite.changeMinMaxInterval(0, 5); 	// Loop from which column to which column?
			sprite.setFlipped(false);
			let ctx = document.getElementById("canvasGame").getContext("2d");

			tick();

			function tick() {
				//Animation entré jeu
			if(nbOtherPlayer > 0)
			{
				enterGameSasuke = true;
				leaveGameSasuke = false;
			}
			else{
				enterGameSasuke = false;
				leaveGameSasuke = true;
			}


			if(positionXSasuke < 170 && enterGameSasuke == true){
				positionXSasuke+=10;
				inGameSasuke = true;
				sprite.setFlipped(false);
				sprite.tick(positionXSasuke, 425, ctx);
			}

				//Variable passive a true
			if(positionXSasuke >160 && positionXSasuke < 180 && inGameSasuke == true)
			{
				enterGameSasuke = false;
			}

				//position passive (STANCE)
			if(positionXSasuke >= 170 && animationMort != "GAME_NOT_FOUND_LOST" && leaveGameSasuke == false && animationSasuke==0){
				sprite.changeRow(1);
				sprite.changeMinMaxInterval(0,3);
				sprite.tick(positionXSasuke, 425, ctx);			
			}	


				//animation d'attaque de Sasuke
			if(animationSasuke > 0 && enterGameSasuke == false){
				sprite.changeRow(2);
				sprite.changeMinMaxInterval(0,10);
				sprite.tick(positionXSasuke, 425, ctx);
				animationSasuke++;
				if(animationSasuke > 65)
				{
					animationSasuke =0;
					
				}
			}

			if(leaveGameSasuke == true && positionXSasuke > -200)
			{
				positionXSasuke-=10;
				inGameSasuke = false;
				sprite.changeRow(0);
				sprite.changeMinMaxInterval(0, 5);
				sprite.setFlipped(true);
				sprite.tick(positionXSasuke, 425, ctx);
				
			}

			if(animationMort == "GAME_NOT_FOUND_LOST"){
				if(animationSasuke < 10){
					animationSasuke++
					sprite.changeRow(3);
					sprite.changeMinMaxInterval(0,6);
					sprite.tick(positionXSasuke, 475, ctx);
				}
				else{
					sprite.changeRow(3);
					sprite.changeMinMaxInterval(6,6);
					sprite.tick(positionXSasuke, 475, ctx);
				}
			}

			



				window.requestAnimationFrame(tick);
			}

});	


