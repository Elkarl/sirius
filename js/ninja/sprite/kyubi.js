let positionXBoss = 1200;
let enterGame = true;
let positionPassif = false;

$(function() {

	// =======================================================
			// Animation in a canvas
			// -------------------------------------------------------
			let columnCount = 10;
			let rowCount = 6;
			let refreshDelay = 100; 			// msec
			let loopColumns = true; 			// or by row?
			let scale = 2.0;
			let sprite = new TiledImage("images/kyubiRunningFixed.png", columnCount, rowCount, refreshDelay, loopColumns, scale, null);
			sprite.changeRow(0);				// One row per animation
			sprite.changeMinMaxInterval(0, 5); 	// Loop from which column to which column?
			sprite.setFlipped(true);
			let ctx = document.getElementById("canvasGame").getContext("2d");

			tick();

			function tick() {
				//Animation entré jeu
			if(positionXBoss > 750 && enterGame == true){
				positionXBoss-=10;
				sprite.tick(positionXBoss, 400, ctx);
			}

				//Variable passive a true
			if(positionXBoss >740 && positionXBoss < 760)
			{
				enterGame = false;
			}

				//position passive (STANCE)
			if(positionXBoss <= 750 && attackedBoss==false && animationMort != "GAME_NOT_FOUND_WIN"){
				sprite.changeRow(1);
				sprite.changeMinMaxInterval(0,5);
				sprite.tick(positionXBoss, 400, ctx);			
			}	

				//animation d'attaque du boss
			if(attackedBoss==true){
				sprite.changeRow(4);
				sprite.changeMinMaxInterval(0,4);
				sprite.tick(positionXBoss, 400, ctx);
			}

			if(animationMort == "GAME_NOT_FOUND_WIN"){
				if(animationAttack < 20){
					animationAttack++
					sprite.changeRow(2);
					sprite.changeMinMaxInterval(0,5);
					sprite.tick(positionXBoss, 400, ctx);
				}
				else{
					sprite.changeRow(2);
					sprite.changeMinMaxInterval(4,4);
					sprite.tick(positionXBoss, 400, ctx);
				}
			}

				window.requestAnimationFrame(tick);
			}

});	


