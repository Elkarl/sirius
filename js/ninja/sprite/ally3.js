let positionXJuggo = -200;
let enterGameJuggo = true;
let positionPassifJuggo = false;
let inGameJuggo =false;
let leaveGameJuggo = false;



$(function() {

	// =======================================================
			// Animation in a canvas
			// -------------------------------------------------------
			let columnCount = 9;
			let rowCount = 5;
			let refreshDelay = 100; 			// msec
			let loopColumns = true; 			// or by row?
			let scale = 1.75;
			let sprite = new TiledImage("images/juggoSprite.png", columnCount, rowCount, refreshDelay, loopColumns, scale, null);
			sprite.changeRow(0);				// One row per animation
			sprite.changeMinMaxInterval(0, 5); 	// Loop from which column to which column?
			let ctx = document.getElementById("canvasGame").getContext("2d");

			tick();

			function tick() {

			if(nbOtherPlayer > 2)
			{
				enterGameJuggo = true;
				leaveGameJuggo = false;
			}
			else{
				enterGameJuggo = false;
				leaveGameJuggo = true;
			}
				//Animation entré jeu
			if(positionXJuggo < 90 && enterGameJuggo == true){
				positionXJuggo+=10;
				inGameJuggo = true;
				sprite.setFlipped(false);
				sprite.tick(positionXJuggo, 400, ctx);
			}

				//Variable passive a true
			if(positionXJuggo >80 && positionXJuggo < 100  && inGameJuggo == true)
			{
				enterGameJuggo = false;
			}

				//position passive (STANCE)
			if(positionXJuggo >= 90 && animationMort != "GAME_NOT_FOUND_LOST"  && leaveGameJuggo == false && animationJuggo==0){
				sprite.changeRow(1);
				sprite.changeMinMaxInterval(0,3);
				sprite.tick(positionXJuggo, 400, ctx);			
			}	

				//animation d'attaque de Juggo
				if(animationJuggo > 0 && enterGameJuggo == false){
					sprite.changeRow(3);
					sprite.changeMinMaxInterval(0,8);
					sprite.tick(positionXJuggo, 400, ctx);
					animationJuggo++;
					if(animationJuggo > 55)
					{
						animationJuggo =0;
						
					}
				}

			if(leaveGameJuggo == true && positionXJuggo > -200)
			{
				positionXJuggo-=10;
				inGameJuggo = false;
				sprite.changeRow(0);
				sprite.changeMinMaxInterval(0, 5);
				sprite.setFlipped(true);
				sprite.tick(positionXJuggo, 400, ctx);
				
			}

			if(animationMort == "GAME_NOT_FOUND_LOST"){
				if(animationJuggo < 10){
					animationJuggo++
					sprite.changeRow(2);
					sprite.changeMinMaxInterval(0,4);
					sprite.tick(positionXJuggo, 435, ctx);
				}
				else{
					sprite.changeRow(2);
					sprite.changeMinMaxInterval(4,4);
					sprite.tick(positionXJuggo, 435, ctx);
				}
			}



				window.requestAnimationFrame(tick);
			}

});	


