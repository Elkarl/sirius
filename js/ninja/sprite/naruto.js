let positionXNaruto = -200;
let enterGameNaruto = true;
let positionPassifNaruto = false;
let rasenShurikenLance = false;


$(function() {

	// =======================================================
			// Animation in a canvas
			// -------------------------------------------------------
			let columnCount = 23;
			let rowCount = 7;
			let refreshDelay = 100; 			// msec
			let loopColumns = true; 			// or by row?
			let scale = 2.0;
			let sprite = new TiledImage("images/narutoSpriteSheetFixed.png", columnCount, rowCount, refreshDelay, loopColumns, scale, null);
			sprite.changeRow(0);				// One row per animation
			sprite.changeMinMaxInterval(0, 5); 	// Loop from which column to which column?
			let ctx = document.getElementById("canvasGame").getContext("2d");

			tick();

			function tick() {
				//Animation entré jeu
			if(positionXNaruto < 250 && enterGame == true){
				positionXNaruto+=10;
				sprite.tick(positionXNaruto, 400, ctx);
			}

				//Variable passive a true
			if(positionXNaruto >240 && positionXNaruto < 260)
			{
				enterGameNaruto = false;
			}

				//position passive (STANCE)
			if(positionXNaruto >= 240 && skill1Bool==false && skill2Bool==false && skill3Bool==false && animationMort != "GAME_NOT_FOUND_LOST"){
				sprite.changeRow(1);
				sprite.changeMinMaxInterval(0,3);
				sprite.tick(positionXNaruto, 400, ctx);			
			}	

				//animation d'attaque de naruto
			if(skill1Bool==true && animationAttack > 0){
				sprite.changeRow(3);
				sprite.changeMinMaxInterval(0,10);
				sprite.tick(positionXNaruto, 400, ctx);
				animationAttack++;
				if(animationAttack > 50)
				{
					animationAttack =0;
					skill1Bool=false;
				}
			}

			if(skill2Bool==true && animationAttack > 0){
				sprite.changeRow(4);
				sprite.changeMinMaxInterval(0,21);
				sprite.tick(positionXNaruto, 400, ctx);
				animationAttack++;
				if(animationAttack > 125)
				{
					animationAttack =0;
					skill2Bool=false;
				}
			}

			if(skill3Bool==true && animationAttack > 0){
				if(animationAttack < 10){
					sprite.changeRow(3);
					sprite.changeMinMaxInterval(0,3);
					sprite.tick(positionXNaruto, 400, ctx);
					animationAttack++;
				}
				else{
					sprite.changeRow(5);
					sprite.changeMinMaxInterval(0,4);
					sprite.tick(positionXNaruto, 400, ctx);
					animationAttack++;
						if(animationAttack > 25)
						{
							
							animationAttack =0;
							skill3Bool=false;
							rasenShurikenLance=true;

						}
					}
			}

			if(animationMort == "GAME_NOT_FOUND_LOST"){
				if(animationAttack < 10){
					animationAttack++
					sprite.changeRow(2);
					sprite.changeMinMaxInterval(0,2);
					sprite.tick(positionXNaruto, 450, ctx);
				}
				else{
					sprite.changeRow(2);
					sprite.changeMinMaxInterval(2,2);
					sprite.tick(positionXNaruto, 450, ctx);
				}
			}



				window.requestAnimationFrame(tick);
			}

});	


