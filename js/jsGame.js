var ctx = null;
let background=null;
var canvasWidth;
var canvasHeight;
let sprite;
let attackedBoss = false
let skill1Bool = false;
let skill2Bool = false;
let skill3Bool = false;
let animationAttack = 0;
let cooldownSkill = Date.now();
let endGame = false;
let animationMort;



let nbOtherPlayer = 0;

let animationSasuke=0;
let animationKizame= 0;
let animationJuggo=0;

let ally1Attack;
let ally2Attack;
let ally3Attack;

let musicGame;
let musicRasenShuriken = new Audio("mp3/RasenShuriken.mp3");



$(function() {

if(endGame ==false){
	setTimeout(function statusRefresh(){
		let gameName;
		let gameType;
		let gameLevel;
		let gameHp;
		let gameMaxHp;
		let playerName;
		let playerLevel;
		let playerType;
		let skillName;
		let skillCost;
		let skillDmg;
		let skillHeal;
		let playerHp;
		let playerMp;
		let playerMaxHp;
		let playerMaxMp;
		let other_playerName;
		let other_playerLevel;
		let welcomeText;
		let other_playerType;
		let other_playerMaxHp;
		let other_playerMaxMp;
		let other_playerHp;
		let other_playerMp;
		$.ajax({
			url:"ajax-game-status.php",
			type: "POST",
			data:{
				"game":{
					"name": gameName,
					"type": gameType,
					"level": gameLevel,
					"hp": gameHp,
					"attacked" : false,
					//"last_target" : "party",
					"max_hp": gameMaxHp
				},
				"player":{
					"name": playerName,
					"level": playerLevel,
					"type": playerType,
					//"victories":0,
					"skills":[
						{
						"name": skillName,
						//"heal_target":null,
						//"level":1,
						"cost": skillCost,
						"dmg": skillDmg,
						"heal": skillHeal
						}
					],
					"hp": playerHp,
					"mp": playerMp,
					"max_hp": playerMaxHp,
					"max_mp": playerMaxMp
				},
				"other_players":[
					{
					"name":other_playerName,
					"level":other_playerLevel,
					"welcome_text":welcomeText,
					"type":other_playerType,
					//"victories":"0",
					"max_hp":other_playerMaxHp,
					"max_mp":other_playerMaxMp,
					"hp":other_playerHp,
					"mp":other_playerMp,
					"attacked":"--"
					}
				]
			}
		})
		.done(function (data){
			setTimeout(statusRefresh,2000);
			let information = JSON.parse(data);
			if(information == "GAME_NOT_FOUND_WIN" || information== "GAME_NOT_FOUND_LOST")
			{
				endGame = true;
				animationMort = information;

				document.getElementById("boxFinPartie").style.display = "block";

				parentNode = document.getElementById("boxFinPartie");
				parentNode.innerHTML = "<div id='boxFinPartie' class='game-box-end'>"; 

				if(information == "GAME_NOT_FOUND_WIN"){ 
					
					document.getElementById("boxFinPartie").innerHTML = "VICTORY ! <br> AN OTHER STEP TOWARD BECOMING HOKAGE !";
				}
				else{
					document.getElementById("boxFinPartie").innerHTML = "DEFEAT ! <br> RIP NARUTO v_v";
				}


				text = 	"<ul>"+
							"<li id='FenetreGame' style='padding:75px;'><a href='home.php'>Return home</a></li>" +
							"<li id='quitterJeu'><a href='logout.php'>Quit</a></li>" +
						"</ul>";
		
						
		
				parentNode.innerHTML = parentNode.innerHTML 
				+ text 
				+ "</div>";		
			}
			else{
				document.getElementById("noMp").style.display = "none";
				document.getElementById("divGameContenu").style.display = "block";
				document.getElementById("infoPartie").innerHTML = information.game.name;

				document.getElementById("playerName").innerHTML = information.player.name;
				document.getElementById("playerLevel").innerHTML = "Level: " + information.player.level;
				document.getElementById("playerType").innerHTML = "Type: " + information.player.type;
				document.getElementById("playerHp").innerHTML = "hp: "+information.player.hp + "/" + information.player.max_hp;
				document.getElementById("playerMp").innerHTML = "mp: "+information.player.mp + "/" + information.player.max_mp;

				if(information.other_players != undefined){
					nbOtherPlayer = information.other_players.length;
					document.getElementById("infoOtherPlayer").style.display = "block";
					parentNode = document.getElementById("infoOtherPlayer");
					parentNode.innerHTML = "<div id='infoOtherPlayer'></div>"; 
					for(let i=0; i<information.other_players.length; i++){
						if(i == 0)
						{
							ally1Attack = information.other_players[i].attacked;
							if(information.other_players[i].attacked != "--")
							{
								animationSasuke =1;
							}
						}
						if(i == 1)
						{
							ally2Attack = information.other_players[i].attacked;
							if(information.other_players[i].attacked != "--")
							{
								animationKizame =1;
							}
						}
						if(i == 2)
						{
							ally3Attack = information.other_players[i].attacked;
							if(information.other_players[i].attacked != "--")
							{
								animationJuggo =1;
							}
						}
						text = 	"<div class='game-box-player' style='top:"+(20*(i+0.5))+"%;background-color:rgba(0,0,0,1);'>"+
							"<div id='otherPlayerName_"+i+"'>" + information.other_players[i].name + "</div>" +
							"<div id='otherPlayerLevel_"+i+"'>" +  "Level: " + information.other_players[i].level + "</div>" +
							"<div id='otherPlayerType_"+i+"'>" +  "Type: " + information.other_players[i].type  +"</div>" +
							"<div id='otherPlayerHp_"+i+"'>"+"hp: "+ information.other_players[i].hp + "/" + information.other_players[i].max_hp +"</div>" +
							"<div id='otherPlayerMp_"+i+"'>"+"mp: "+ information.other_players[i].mp + "/" + information.other_players[i].max_mp +"</div>"+
							"</div>";
	

					
					
	
						parentNode.innerHTML = parentNode.innerHTML 
						+ text 
						+ "</div>";		
					}
				}
				else
				{
					document.getElementById("infoOtherPlayer").style.display = "none";	
				}
				for(let i=0; i<information.player.skills.length; i++){
					if(information.player.skills[i] != undefined){
						document.getElementById("skill"+i).style.display = "block";
						document.getElementById("skill"+i+"Name").innerHTML = information.player.skills[i].name;
						document.getElementById("skill"+i+"Cost").innerHTML = "Mana cost: "+information.player.skills[i].cost;
						document.getElementById("skill"+i+"Dmg").innerHTML = "Dmg: " + information.player.skills[i].dmg;
						document.getElementById("skill"+i+"Heal").innerHTML = "Heal: " + information.player.skills[i].heal;
					}
				}

				document.getElementById("gameType").innerHTML = information.game.type;
				document.getElementById("gameLevel").innerHTML = "Level: "+information.game.level;
				document.getElementById("gameHp").innerHTML = information.game.hp + "/" + information.game.max_hp;
				//skills are undefined yet
				attackedBoss = information.game.attacked;
			}



		})
	},2000)

		document.getElementById("skill0").onclick = function(){
			setTimeout(function(){
				let skill = "Normal";
				//console.log(Date.now() > cooldownSkill + 3000 );
				if(Date.now() > cooldownSkill + 2000 )
				{
					$.ajax({
						url:"ajax-game-skills.php",
						type: "POST",
						data:{
							"skill-name" : skill
						}

					})
					.success(function (data){
						// setTimeout(skill0,2000);
						let information = JSON.parse(data);
						cooldownSkill = Date.now();
						if (information == "OK")
						{
							skill1Bool = true;
							animationAttack = 1;
						}
							
					})
				}
			},2000)
		
		}

		document.getElementById("skill1").onclick = function(){
			setTimeout(function(){
				let skill = "Special1";
				if(Date.now() > cooldownSkill + 2000 )
				{
					$.ajax({
						url:"ajax-game-skills.php",
						type: "POST",
						data:{
							"skill-name" : skill
						}

					})
					.done(function (data){
						// setTimeout(skill1,2000);
						let information = JSON.parse(data);
						cooldownSkill = Date.now();
						if (information == "OK")
							{
								skill2Bool = true;
								animationAttack = 1;
							}
						if(information == "NOT_ENOUGH_MP")
						{
							document.getElementById("noMp").style.display = "block";
							document.getElementById("noMp").innerHTML = "Not enough chakra left";
						}	
					})
				}
			},2000)
		
		}

		document.getElementById("skill2").onclick = function(){
			setTimeout(function(){
				let skill = "Special2";
				if(Date.now() > cooldownSkill + 2000 )
				{
					$.ajax({
						url:"ajax-game-skills.php",
						type: "POST",
						data:{
							"skill-name" : skill
						}

					})
					.done(function (data){
						// setTimeout(skill2,2000);
						let information = JSON.parse(data);
						cooldownSkill = Date.now();
						if (information == "OK")
							{
								musicRasenShuriken.play();
								musicRasenShuriken.volume =  0.02;
								skill3Bool = true;
								animationAttack = 1;
							}
						if(information == "NOT_ENOUGH_MP")
							{
								document.getElementById("noMp").style.display = "block";
								document.getElementById("noMp").innerHTML = "Not enough chakra left";
							}	
							
					})
				}
			},2000)
		
		}
}

	

	

	ctx = document.getElementById("canvasGame").getContext("2d");
	background = new Image();
	background.src = "images/landscape8bit.png";

	
	musicGame = new Audio("mp3/Madara.mp3");
	musicGame.play();
	musicGame.volume =  0.05;

	generalTick();
	
});	

// document.getElementById("bouttonSubmit").onclick = function(){
// 	console.log("1");
// 	localStorage["username"] = $("#textAreaUsername").val();
// }


function generalTick() {
	if (background.complete) {
		ctx.drawImage(background, 0, 0);
	}
	cooldown = ((cooldownSkill-Date.now())/(-20));
	// console.log(cooldown/(-20));
	if(cooldown <= 110)
	{
		$( function() {
			var progressbar = $( "#progressbar" ),
			  progressLabel = $( ".progress-label" );
		 
			progressbar.progressbar({
			  value: cooldown,
			  change: function() {
				progressLabel.text( progressbar.progressbar( "value" ) + "%" );
			  },
			  complete: function() {
				progressLabel.text( "YOU CAN ATTACK !" );
			  }
			});
		 
			function progress() {
			  var val = progressbar.progressbar( "value" ) || 0;
		 
			  progressbar.progressbar( "value", val + 2 );
		 
			  if ( val < 99 ) {
				setTimeout( progress, 80 );
			  }
			}
		 
			setTimeout( progress, 2000 );
		  } );
	}
	if(musicGame.ended)
	{
		musicGame.play();
	}
	window.requestAnimationFrame(generalTick);
}



window.onresize = function(event) {
};


